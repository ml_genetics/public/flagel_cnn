import sys
import os
import numpy as np
from tensorflow import keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Dense, Dropout, Flatten
from tensorflow.keras.layers import concatenate
from tensorflow.keras.layers import Conv2D, Conv1D
from tensorflow.keras.layers import MaxPooling2D, AveragePooling1D
from sklearn.neighbors import NearestNeighbors
import utils
import pandas as pd

batch_size = 200
epochs = 10
nindiv = 50
numParams = 21
NUM_GPU = 1

(encoding, maxDistance, SNP_max, preprocessed_scenario_params_path,
 convDim, convSize, poolSize, useLog, useInt, sortRows,
 useDropout, lossThreshold, inDir, weightFileName,
 modFileName, testPredFileName) = sys.argv[1:]

convDim = convDim.lower()
convSize, poolSize = int(convSize), int(poolSize)
useLog = True if useLog.lower() in ["true", "logy"] else False
useInt = True if useInt.lower() in ["true", "intallele"] else False
sortRows = True if sortRows.lower() in ["true", "sortrows"] else False
useDropout = True if useDropout.lower() in ["true", "dropout"] else False
lossThreshold = float(lossThreshold)
SNP_max = int(SNP_max)
NUM_GPU = int(NUM_GPU)
maxDistance = int(maxDistance)


def resort_min_diff(amat):
    # assumes your snp matrix is indv. on rows, snps on cols
    mb = NearestNeighbors(len(amat), metric='manhattan', n_jobs=1).fit(amat)
    v = mb.kneighbors(amat)
    smallest = np.argmin(v[0].sum(axis=1))
    return amat[v[1][smallest]]

# Load all data, create dataloader:
class DataGenerator(keras.utils.Sequence):

    def __init__(self, indices, scen_params_preprocessed, batchSize,
                 SNP_maxx, sort_rows, encodingg, max_distance):
        self.indices = indices
        self.scenario_params_preprocessed = scen_params_preprocessed
        self.batch_size = batchSize
        self.SNP_max = SNP_maxx
        self.sortRows = sort_rows # True/False
        self.encoding = encodingg # "255/1"
        self.maxDistance = max_distance # max distance btw snps

    def __len__(self): # number of batches
        return int(np.ceil(len(self.indices) / float(self.batch_size)))

    def __getitem__(self, idx): # should return entire batch
        batch_snp = []
        batch_pos = []
        batch_demo = []
        for index in range(idx, idx+batch_size):
            scenario_idx, npz_path = self.indices[index]
            scenario_data = scenario_params_preprocessed.loc[scenario_idx].copy()

            with np.load(npz_path) as data_npz:
                pos = (((data_npz['POS'] / self.maxDistance) / 2e6) * 2) - 1 # positions between -1 and 1
                snp = data_npz["SNP"]
            snp = resort_min_diff(snp) # sort matrix
            if self.encoding == "1":
                snp = snp.astype("float")*2 - 1 # between -1,1
            else:
                snp = (snp*255).astype('int8') # 0->0 ; 255 -> -1

            if SNP_max == 400: # truncate up to SNP_max

                batch_pos.append(pos[:SNP_max])
                new_snp = snp[:, :SNP_max]
                batch_snp.append(new_snp.T)

            else: # need to pad up to 17839 (max number of snp in Theophile's simulations)
                # snp_2_keep = np.sort(np.random.choice(snp.shape[1],
                #                                int(snp.shape[1]/10), # take about 10% of the initial snps
                #                                replace=False))
                # subsample as in flagel's code:
                snp_2_keep = np.arange(snp.shape[1])[::10]
                snp = snp[:, snp_2_keep]
                p_tmp = pos.cumsum()[snp_2_keep] # subsample in the absolute distance space
                pos = np.r_[p_tmp[0], np.diff(p_tmp)] # recompute the diff and concat (r_) with first value

                new_pos = np.zeros(SNP_max)
                new_pos[:pos.shape[0]] = pos
                new_snp = np.zeros((snp.shape[0], SNP_max))
                new_snp[:snp.shape[0], :snp.shape[1]] = snp
                batch_pos.append(new_pos)
                batch_snp.append(new_snp.T)

            batch_demo.append(scenario_data.filter(regex="pop.*size").values) # 21 demo parameters
            
        return (np.array(batch_snp), np.array(batch_pos)), np.array(batch_demo)


scenario_params_preprocessed = pd.read_csv(preprocessed_scenario_params_path, index_col="scenario_idx")
# Test that there is no information leakage
# shuffle ouput sequences
#scenario_params_preprocessed.iloc[:, 27:-1] = scenario_params_preprocessed.iloc[:, 27:-1].apply(lambda x: x.sample(frac=1).values) 

num_scenario = len(scenario_params_preprocessed)
simu_data_path = "/home/tau/thsanche/data/"
print("Building indices...")
training_indices = []
validation_indices = []
for scenario in scenario_params_preprocessed.itertuples():
    for k in range(scenario.num_replicates):

        data = (scenario.Index,
                os.path.join(simu_data_path,
                            utils.generate_filename((scenario.Index, k),
                                                   "cattle",
                                                   20000, # not used
                                                   scenario.num_replicates,
                                                   return_abs_path=True,
                                                   format_scen="",
                                                   format_rep="",
                                                   )
                            )
               )
        if scenario.training_set:
            training_indices.append(data)
        else:
            validation_indices.append(data)

print("\n".join([i[1] for i in training_indices[:5]]))
print("\n".join([i[1] for i in validation_indices[:5]]))
print("shuffling indices...")
np.random.shuffle(training_indices)
np.random.shuffle(validation_indices)
print("\n".join([i[1] for i in training_indices[:5]]))
print("\n".join([i[1] for i in validation_indices[:5]]))

my_training_batch_generator = DataGenerator(training_indices,
                                            scenario_params_preprocessed,
                                            batch_size,
                                            SNP_max,
                                            sort_rows=True,
                                            encodingg=encoding,
                                            max_distance=maxDistance) # 1082459 from sort -k4,4gr cattle_data_clean_more400SNP.csv | head
my_validation_batch_generator = DataGenerator(validation_indices,
                                              scenario_params_preprocessed,
                                              batch_size,
                                              SNP_max,
                                              sort_rows=True,
                                              encodingg=encoding,
                                              max_distance=maxDistance) # 1082459 from sort -k4,4gr cattle_data_clean_more400SNP.csv | head


# What about padding?

# X = []
# y = []
# print("reading data")
# for npzFileName in os.listdir(inDir):
#     u = np.load(inDir + npzFileName)
#     currX, curry = [u[i] for i in ('X', 'y')]
#     ni, nr, nc = currX.shape # replicate, row, col
#     newCurrX = []
#     for i in range(ni):
#         currCurrX = [currX[i, 0]] # pos ?
#         if sortRows:
#             currCurrX.extend(resort_min_diff(currX[i, 1:]))
#         else:
#             currCurrX.extend(currX[i, 1:])
#         currCurrX = np.array(currCurrX)
#         newCurrX.append(currCurrX.T)
#     currX = np.array(newCurrX)
#     assert currX.shape == (ni, nc, nr)
#     #indices = [i for i in range(nc) if i % 10 == 0]
#     # X.extend(np.take(currX,indices,axis=1))
#     X.extend(currX)
#     y.extend(curry)
#     # if len(y) == 10000:
#     #    break

# y = np.array(y)
# numParams = y.shape[1]
# if useLog:
#     y[y == 0] = 1e-6  # give zeros a very tiny value so they don't break our log scaling
#     y = np.log(y)
# totInstances = len(X)
# testSize = 2
# valSize = 2
# print("formatting data arrays")
# X = np.array(X)
# posX = X[:, :, 0]
# X = X[:, :, 1:]
# imgRows, imgCols = X.shape[1:]
#
# if useInt:
#     X = X.astype('int8') # Bug, should be uint8 otherwise int8(255) -> -1
# else:
#     X = X.astype('float32') / 127.5 - 1
# if convDim == "2d":
#     X = X.reshape(X.shape[0], imgRows, imgCols, 1).astype('float32')
# posX = posX.astype('float32') / 127.5 - 1
#
# assert totInstances > testSize + valSize
#
# testy = y[:testSize]
# valy = y[testSize:testSize + valSize]
# y = y[testSize + valSize:]
# testX = X[:testSize]
# testPosX = posX[:testSize]
# valX = X[testSize:testSize + valSize]
# valPosX = posX[testSize:testSize + valSize]
# X = X[testSize + valSize:]
# posX = posX[testSize + valSize:]
#
# yMeans = np.mean(y, axis=0)
# yStds = np.std(y, axis=0)
# y = (y - yMeans) / yStds
# testy = (testy - yMeans) / yStds
# valy = (valy - yMeans) / yStds
#
# print("lenX, y, yMeans: ", len(X), len(y), len(yMeans))
# print("yMeans, yStds :", yMeans, yStds)
# print("X.shape, testX.shape, valX.shape : ", X.shape, testX.shape, valX.shape)
# print("posX.shape, testPosX.shape, valPosX.shape : ",
#        posX.shape, testPosX.shape, valPosX.shape)
# print("y.shape, valy.shape : ", y.shape, valy.shape)
# print("ready to learn (%d params, %d training examples, %d rows, %d cols)" %
#       (numParams, len(X), imgRows, imgCols))

if convDim == "2d":
    inputShape = (SNP_max, nindiv, 1)
    convFunc = Conv2D
    poolFunc = MaxPooling2D
else:
    inputShape = (SNP_max, nindiv)
    convFunc = Conv1D
    poolFunc = AveragePooling1D

b1 = Input(shape=inputShape)
conv11 = convFunc(128, kernel_size=convSize, activation='relu')(b1)
pool11 = poolFunc(pool_size=poolSize)(conv11)
if useDropout:
    pool11 = Dropout(0.25)(pool11)
conv12 = convFunc(128, kernel_size=2, activation='relu')(pool11)
pool12 = poolFunc(pool_size=poolSize)(conv12)
if useDropout:
    pool12 = Dropout(0.25)(pool12)
conv13 = convFunc(128, kernel_size=2, activation='relu')(pool12)
pool13 = poolFunc(pool_size=poolSize)(conv13)
if useDropout:
    pool13 = Dropout(0.25)(pool13)
conv14 = convFunc(128, kernel_size=2, activation='relu')(pool13)
pool14 = poolFunc(pool_size=poolSize)(conv14)
if useDropout:
    pool14 = Dropout(0.25)(pool14)
flat11 = Flatten()(pool14)

b2 = Input(shape=(SNP_max,))
dense21 = Dense(32, activation='relu')(b2)
if useDropout:
    dense21 = Dropout(0.25)(dense21)

merged = concatenate([flat11, dense21])
denseMerged = Dense(256, activation='relu',
                    kernel_initializer='normal')(merged)
if useDropout:
    denseMerged = Dropout(0.25)(denseMerged)
denseOutput = Dense(numParams)(denseMerged)
model = Model(inputs=[b1, b2], outputs=denseOutput)
print(model.summary())

if NUM_GPU != 1:
    model = keras.utils.multi_gpu_model(model, gpus=NUM_GPU)

model.compile(loss='mean_squared_error', optimizer='adam')
earlystop = keras.callbacks.EarlyStopping(
    monitor='val_loss', min_delta=0, patience=3, verbose=0, mode='auto')
checkpoint = keras.callbacks.ModelCheckpoint(
    weightFileName, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
callbacks = [earlystop, checkpoint]

# print("X: ", X.shape)
# print("posX: ", posX.shape)
# print("y: ", y.shape)
# print("valX: ", valX.shape)
# print("valPosX: ", valPosX.shape)
# print("valy: ", valy.shape)

if __name__ == "__main__":

    model.fit_generator(generator=my_training_batch_generator,
                    steps_per_epoch = int(len(training_indices) // batch_size),
                    epochs = epochs,
                    verbose = 1,
                    validation_data = my_validation_batch_generator,
                    validation_steps = int(len(validation_indices) // batch_size),
                    callbacks=callbacks)
    print("done")
# model.fit([X, posX], y, batch_size=batch_size, epochs=epochs, verbose=1,
#           validation_data=([valX, valPosX], valy), callbacks=callbacks)

# now we load the weights for the best-performing model
#model.load_weights(weightFileName)
#model.compile(loss='mean_squared_error', optimizer='adam')

# now we get the loss for our best model on the test set and emit predictions
# testLoss = model.evaluate_generator(generator=my_validation_batch_generator)
# print(testLoss)
# preds = model.predict([testX, testPosX])
# with open(testPredFileName, "w") as outFile:
#     for i in range(len(preds)):
#         outStr = []
#         for j in range(len(preds[i])):
#             outStr.append("%f vs %f" % (testy[i][j], preds[i][j]))
#         outFile.write("\t".join(outStr) + "\n")
#
# # if the loss is lower than our threshold we save the model file if desired
# if modFileName.lower() != "nomod" and testLoss <= lossThreshold:
#     with open(modFileName, "w") as modFile:
#         modFile.write(model.to_json())
# else:
#     os.system("rm %s" % (weightFileName))
