#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import builtins
import json
import numpy as np
import os
import pandas as pd
import zipfile


def print(*args):
    """Overwrite the print function so flush is always true.

    It prevents print function from using the buffer and prevent buffer issues of slurm.
    """
    builtins.print(*args, flush=True)

def count_parameters(model):
    """Count the number of learnable parameter in an object of the :class:torch.nn.Module: class.

    Arguments:
        model (nn.Module):
    """
    return sum(p.numel() for p in model.parameters() if p.requires_grad)

def load_dict_from_json(filepath):
    """Load a json in a dictionnary.

    The dictionnary contains the overall parameters used for the simulation (e.g. path to the data folder, number of epoch). The json can be created from a dictionnary using ``save_dict_in_json``.

    Arguments:
        filepath (string): filepath to the json file
    """
    return json.loads(open(filepath).read())

def save_dict_in_json(filepath, params):
    """Save a dictionnary into a json file.

    Arguments:
        filepath (string): filepath of the json file
        params(dict): dictionnary containing the overall parameters used for the simulation (e.g. path to the data folder, number of epoch...)
    """
    with open(filepath, 'w') as file_handler:
        json.dump(params,
                  file_handler,
                  indent=4) # for pretty printing.

def standardize(parameters, mean, std):
    """Standardize the demographic parameters following :math:`\theta = \frac{\theta - \mu}{\sigma}.`

    Means and standard deviation of each parameters should be precomputed over the training set.

    Arguments:
        parameters (torch.Tensor or numpy.ndarray): :math:`n \times m` parameters to standardize
        mean (torch.Tensor or numpy.ndarray): mean of each parameter computed over the training in a vector of size :math:`m`
        std (torch.Tensor or numpy.ndarray): standard deviation of each parameter computed over the training in a vector of size :math:`m`
    """
    return ((parameters - mean) / std)

def cut_and_cat(tensor1, tensor2):
    """
    To describe.
    """
    if tensor1.size() == torch.Tensor([]).size():
        return tensor2
    elif tensor2.size() == torch.Tensor([]).size():
        return tensor1
    dif = tensor1.size(2) - tensor2.size(2)

    if dif > 0:
        return torch.cat((tensor1[:,:,dif//2:-dif//2], tensor2), 1)
    elif dif < 0:
        return torch.cat((tensor1, tensor2[:,:,-dif//2:dif//2]), 1)
    else:
        return torch.cat((tensor1, tensor2), 1)

def generate_filename(index, model_name, num_scenario, num_replicates,
                      return_abs_path=False, format_scen=None, format_rep=None,
                      prefix_scenario="scenario"):
    """Given parameters of a simulation, returns the name of the npz file.
        If return_abs_path is True, it returns the full path

    Parameters
    ----------
    index : int, or tuple
        i-th simulation. If tuple: (scen_id, rep_id).
    model_name : str
        Name of the population genetic model.
    num_scenario : int
        Number of scenario in the given model.
    num_replicates : int
        Number of replicates per scenario.
    return_abs_path : bool
        Whether to return the full path from the model directory (True) or just the filename (False).
    format_scen: str
        If not None, pass a string to set the format of the scenario identifier in the filename.
        e.g. "0>3"
    format_rep: str
        If not None, pass a string to set the format of the replicate identifier in the filename.
        e.g. "0>2"
    prefix_scenario: str
        By default the prefix string is "scenario" in "scenario_000"

    Returns
    -------
    str
        Path to or only filename of the npz file.

    """
    if isinstance(index, tuple):
        scen_id, rep_id = index
    else:
        scen_id = index // num_replicates
        rep_id = index % num_replicates
    if format_scen == None:
        format_scen = "0>" + str(np.ceil(np.log10(num_scenario)).astype(int))
    if format_rep == None:
        format_rep = "0>" + str(np.ceil(np.log10(num_replicates)).astype(int))
    scenario_dir = '{}_{:{}}'.format(prefix_scenario, scen_id, format_scen)

    npz_file = '{}_{:{}}_{:{}}.npz'.format(model_name,
                                           scen_id,
                                           format_scen,
                                           rep_id,
                                           format_rep)

    if return_abs_path:
        return os.path.join(model_name,
                            scenario_dir,
                            npz_file)
    else:
        return npz_file

def rescale(tensors, means, stds, param_2_learn, param_2_log, label=""):
    """Rescale a tensor of shape (n x p) by reversing the mean centering and standardization.
    returns tensor * stds + means

    Parameters
    ----------
    tensors : torch.Tensor
        predicted or target values.
    means : pd.Series
        Series with the mean for the different learned parameters.
    stds : pd.Series
        Series with the standard deviation for the different learned parameters.
    param_2_learn : list
        List of learned parameters of size (p,).
    param_2_log : list
        List of paramaters to get the exponential from.
    label : str
        String to append to the name of the columns which will be the name of the parameters.

    Returns
    -------
    pd.DataFrame
        Dataframe with the parameters in columns.

    """

    tmp = pd.DataFrame(tensors.detach().cpu().numpy(), columns=param_2_learn)
    tmp_rescale = tmp * stds + means
    for pname in param_2_learn:
        if pname in param_2_log:
            tmp_rescale[pname] = np.exp(tmp_rescale[pname])
    if label != "":
        tmp_rescale.columns = ["_".join([i, label]) for i in tmp_rescale.columns]
    return tmp_rescale


def get_shape_npz(npz):
    """Takes a path to an .npz file, which is a Zip archive of .npy files.
    Generates a dict like {name: shape}.
    """
    with zipfile.ZipFile(npz) as archive:
        for name in archive.namelist():
            if not name.endswith('.npy'):
                continue

            npy = archive.open(name)
            version = np.lib.format.read_magic(npy)
            shape, fortran, dtype = np.lib.format._read_array_header(npy, version)
            yield {name[:-4]: shape}

DEBUG_LEVELV_NUM = 5
def debugv(self, message, *args, **kws):
    if self.isEnabledFor(DEBUG_LEVELV_NUM):
        # Yes, logger takes its '*args' as 'args'.
        self._log(DEBUG_LEVELV_NUM, message, args, **kws)
