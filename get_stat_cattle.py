import pandas as pd
import numpy as np
import multiprocessing

def get_all_shapes(npzfile):
    with np.load(npzfile) as npz:
        pos = npz["POS"]
    if len(pos)>1:
        mpos = np.max(pos)
        shape = len(pos)
        return npzfile, int(shape), int(mpos)
    else:
        return npzfile, np.nan, np.nan


all_npz_files = ['/home/tau/thsanche/data/cattle/scenario_{}/cattle_{}_{}.npz'.format(s, s, r)  for s in range(50000) for r in range(100)]


for i in range(0, len(all_npz_files), 500000):
    with multiprocessing.Pool(20) as pool:
        data = pool.map(get_all_shapes, all_npz_files[i:i+500000])
        pool.close()
        pool.join()

    with open("/home/tau/jcury/flagel_cnn/cattle_data.csv", "a") as catdat:
        pd.DataFrame(data, columns=["path", "N_snp", "max_dist"]).to_csv(catdat, index=False, sep="\t", header=catdat.tell()==False)

